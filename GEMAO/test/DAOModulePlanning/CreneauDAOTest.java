/**
 * 
 */
package DAOModulePlanning;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import fr.gemao.entity.Jour;
import fr.gemao.entity.cours.Salle;
import fr.gemao.entity.planning.Creneau;
import fr.gemao.entity.util.HeurePlanning;
import fr.gemao.sql.planning.CreneauDAO;
import static org.mockito.Mockito.*; 

/**
 * @author FELTON-GROS-METAYER-PIAT-VAREILLE
 *
 */
public class CreneauDAOTest {
	
	private Creneau creneau;
	private Jour jour;
	private Salle salle;
	private HeurePlanning debut;
	private HeurePlanning fin;
	private HeurePlanning duree;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		jour = new Jour(2, "Mardi");
		salle = new Salle(1, "Salle1");
		debut = new HeurePlanning(15, 30);
		fin = new HeurePlanning(17, 30);
		duree = new HeurePlanning(2, 0);
		creneau = new Creneau(1, "Piano", debut, duree, 1, salle , jour, null);
		
		MockitoAnnotations.initMocks(this);
		CreneauDAO creneauDAO = Mockito.mock(CreneauDAO.class);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testMethodeCreate(){
	}

}
