package metierModulePlanning;
/**
 * @author FELTON-GROS-METAYER-PIAT-VAREILLE
 *
 */

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import fr.gemao.entity.Jour;
import fr.gemao.entity.cours.Salle;
import fr.gemao.entity.planning.Creneau;
import fr.gemao.entity.util.HeurePlanning;

public class CreneauTest {

	private Creneau creneau;
	private Creneau creneauClone;
	private Jour jour;
	private Salle salle;
	private HeurePlanning debut;
	private HeurePlanning fin;
	private HeurePlanning duree;
	/**
	* Initialisation du contexte global avant l'ensemble des tests
	*/
	@BeforeClass
	public static void setUpGlobal() {
	}
	
	/**
	* Nettoyage global après l'ensemble des tests
	*/
	@AfterClass
	public static void tearDownGlobal() {
	}
	
	/**
	* Initialisation du contexte avant chaque test
	*/
	@Before
	public void setUp() {
		jour = new Jour(2, "Mardi");
		salle = new Salle(1, "Salle1");
		debut = new HeurePlanning(15, 30);
		fin = new HeurePlanning(17, 30);
		duree = new HeurePlanning(2, 0);
		
		creneau = new Creneau(1, "Piano", debut, duree, 1, salle , jour, "Bleu");
	}
	
	/**
	* Libération du contexte après chaque test
	*/
	@After
	public void tearDown() {
	}
	
	@Test
	public void testConstructeurCouleurNull(){
		Creneau creneau2 = new Creneau(1, "Piano", debut, duree, 1, salle , jour, null);
		assertTrue(creneau2.getCouleur() == "ffffff");
	}
	
	@Test
	public void testGetNbQuartHeuresEgal8() {
		assertEquals(creneau.getNbQuartDHeure(), 8);
	}
	
	@Test
	public void testGetHeureFinEgal17h30(){
		System.out.println(creneau.getHeureFin());
		assertTrue(creneau.getHeureFin().equals(fin));
	}

	@Test
	public void testClone(){
		creneauClone = creneau.clone();
		assertTrue(creneau.equals(creneauClone));
	}
}
