/**
 * 
 */
package metierModulePlanning;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import fr.gemao.entity.util.HeurePlanning;

public class HeurePlanningTest {

	private HeurePlanning hp;
	private HeurePlanning hp2;
	
	/**
	* Initialisation du contexte global avant l'ensemble des tests
	*/
	@BeforeClass
	public static void setUpGlobal() {
	}
	
	/**
	* Nettoyage global après l'ensemble des tests
	*/
	@AfterClass
	public static void tearDownGlobal() {
	}
	
	/**
	* Initialisation du contexte avant chaque test
	*/
	@Before
	public void setUp() {
		hp = new HeurePlanning(15, 30);
	}
	
	/**
	* Libération du contexte après chaque test
	*/
	@After
	public void tearDown() {
	}

	@Test
	public void testRetourneHeureEgal15() {
		assertEquals(hp.getHeure(), 15);
	}
	
	@Test
	public void testRetourneMinutesEgal30() {
		assertEquals(hp.getMinute(), 30);
	}
	
	@Test
	public void testRetourneHeureAjoutDureeMinutesSup60() { 
		HeurePlanning duree = new HeurePlanning(2, 45);
		HeurePlanning newDuree = new HeurePlanning(18, 15);
		System.out.println(HeurePlanning.getHeureAjoutDuree(hp, duree));
		 
		assertEquals(HeurePlanning.getHeureAjoutDuree(hp, duree), newDuree);
	}
	
	@Test
	public void testRetourneHeureAjoutDureeMinutesEgal60() { 
		HeurePlanning duree = new HeurePlanning(2, 30);
		HeurePlanning newDuree = new HeurePlanning(18, 00);
		System.out.println(HeurePlanning.getHeureAjoutDuree(hp, duree));
		 
		assertEquals(HeurePlanning.getHeureAjoutDuree(hp, duree), newDuree);
	}
	
	@Test
	public void testEqualsVraiMemeObjet(){
		assertTrue(hp.equals(hp));
	}
	
	@Test 
	public void testEqualsFauxClasseDifferente(){
		String test = "";
		assertFalse(test.equals(hp));
	}
	
	@Test 
	public void testEqualsFauxHeureDifferente(){
		hp2 = new HeurePlanning(16, 30);
		assertFalse(hp2.equals(hp));
	}
	
	@Test 
	public void testEqualsFauxMinuteDifferente(){
		hp2 = new HeurePlanning(15, 15);
		assertFalse(hp2.equals(hp));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testConstructeurHeureInf0(){
		hp = new HeurePlanning(-8, 15);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testConstructeurHeureSup23(){
		hp = new HeurePlanning(29,15);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testConstructeurMinutesInf0(){
		hp = new HeurePlanning(15,-9);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testConstructeurMinutesSup59(){
		hp = new HeurePlanning(15,78);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testConstructeurMinutesNotMod15(){
		hp = new HeurePlanning(15,17);
	}

}

