/**
 * 
 */
package metierModulePlanning;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.gemao.entity.Jour;
import fr.gemao.entity.cours.Salle;
import fr.gemao.entity.planning.Creneau;
import fr.gemao.entity.planning.Planning;
import fr.gemao.entity.util.HeurePlanning;

/**
 * @author FELTON-GROS-METAYER-PIAT-VAREILLE
 *
 */
public class PlanningTest {
	
	private Planning planning;
	private Planning planning2;
	private List<Creneau> listeCreneaux;
	private Date dateDeb;
	private Date dateFin;
	
	/**
	* Initialisation du contexte global avant l'ensemble des tests
	*/
	@BeforeClass
	public static void setUpGlobal() {
	}
	
	/**
	* Nettoyage global après l'ensemble des tests
	*/
	@AfterClass
	public static void tearDownGlobal() {
	}
	
	/**
	* Initialisation du contexte avant chaque test
	 * @throws ParseException 
	*/
	@Before
	public void setUp() throws ParseException {
		String string_date = "14-12-2015";
		SimpleDateFormat deb = new SimpleDateFormat("dd-MM-yyyy");
		java.util.Date dDeb = deb.parse(string_date);
		long millisecondsDeb = dDeb.getTime();
		
		String string_date2 = "20-12-2015";
		SimpleDateFormat fin = new SimpleDateFormat("dd-MM-yyyy");
		java.util.Date dFin = fin.parse(string_date2);
		long millisecondsFin = dFin.getTime();
		
		dateDeb = new Date(millisecondsDeb);
		dateFin = new Date(millisecondsFin);
		listeCreneaux = new ArrayList<>();
		listeCreneaux.add(new Creneau(1, "Creneau", new HeurePlanning(2, 15), new HeurePlanning(2, 0), 1, 
				new Salle(1, "Piano"), new Jour(1, "Lundi"), "rouge"));
		planning = new Planning(1, "PlaningTest", listeCreneaux, dateDeb, dateFin, 50, false);
	}
	
	/**
	* Libération du contexte après chaque test
	*/
	@After
	public void tearDown() {
	}
	
	
	@Test
	public void testSemainePairVrai() {
		assertTrue(planning.isPair());
	}
	
	@Test
	public void testSemainePairFaux() {
		Planning planning2 = new Planning(1, "PlaningTest", listeCreneaux, dateDeb, dateFin, 51);
		assertFalse(planning2.isPair());
	}
	
	@Test
	public void testDefinitionPlanning(){
		assertEquals(planning.definitionPlanning(), "PlaningTest du 2015-12-14 au 2015-12-20");
	}
	
	@Test
	public void testConstructeurCalculantLeNumroDeSemaine(){
		Planning planning = new Planning(1, "PlaningTest", listeCreneaux, dateDeb, dateFin);
		System.out.println(planning.isPair());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testSetIdPlanningIdNegatif(){
		planning.setIdPlanning(-5);
	}
	
	@Test
	public void testSetIdPlanningOk(){
		planning.setIdPlanning(5);
		assertEquals(planning.getIdPlanning(), (Integer)5);
	}
	
	@Test
	public void testMethodeClone(){
		planning2 = planning.clone();
		assertEquals(planning.getNomPlanning(), planning2.getNomPlanning());
	}
	
	@Test
	public void testPlanningValide(){
		planning.setValide(true);
		assertTrue(planning.getValide());
	}

}
